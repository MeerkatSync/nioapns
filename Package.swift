// swift-tools-version:5.1
//
// Edited:
//  author: Filip Klembara (filip@klembara.pro)
//  date:   2. Mar. 2020
//  modifications: Updated swift version and platform
//
import PackageDescription

let package = Package(
    name: "nio-apns",
    products: [
        .library(
            name: "NIOAPNS",
            targets: ["NIOAPNS"]
        ),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "NIOAPNS",
            dependencies: ["CAPNSOpenSSL"]
        ),
        .systemLibrary(
            name: "CAPNSOpenSSL",
            pkgConfig: "openssl"/*,
            providers: [
                .apt(["openssl libssl-dev"]),
                .brew(["openssl"])
            ]*/
        ),
        .testTarget(
            name: "NIOAPNSTests",
            dependencies: ["NIOAPNS"]
        ),
    ]
)
