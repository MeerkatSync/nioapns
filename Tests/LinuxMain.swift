import XCTest

import NIOAPNSTests

var tests = [XCTestCaseEntry]()
tests += NIOAPNSTests.allTests()
XCTMain(tests)
